interface SoundGroup {
  title: string;
  sounds: string[];
}

enum SoundEnum {
  aigle = 'aigle',
  baleine = 'baleine',
  bourdon = 'bourdon',
  canard = 'canard',
  chameau = 'chameau',
  chauveSouris = 'chauve souris',
  chouette = 'chouette',
  cigales = 'cigales',
  corneille = 'corneille',
  criquet = 'criquet',
  crocodile = 'crocodile',
  dindon = 'dindon',
  faucon = 'faucon',
  furet = 'furet',
  guepard = 'guepard',
  leopard = 'leopard',
  lion2 = 'lion 2',
  manchot = 'manchot',
  moustique = 'moustique'
}

export const soundsGroups: SoundGroup[] = [
  {
    title: 'Niveau 1',
    sounds: [
      SoundEnum.baleine,
      SoundEnum.canard,
      SoundEnum.chameau,
      SoundEnum.dindon,
      SoundEnum.furet
    ]
  },
  {
    title: 'Niveau 2',
    sounds: [
      SoundEnum.aigle,
      SoundEnum.baleine,
      SoundEnum.chouette,
      SoundEnum.corneille,
      SoundEnum.faucon,
      SoundEnum.manchot
    ]
  },
  {
    title: 'Niveau 3',
    sounds: [
      SoundEnum.baleine,
      SoundEnum.bourdon,
      SoundEnum.chauveSouris,
      SoundEnum.cigales,
      SoundEnum.criquet,
      SoundEnum.moustique
    ]
  },
  {
    title: 'Niveau 4',
    sounds: [
      SoundEnum.baleine,
      SoundEnum.crocodile,
      SoundEnum.guepard,
      SoundEnum.leopard,
      SoundEnum.lion2
    ]
  },
  {
    title: 'Version peronnalisée',
    sounds: Object.values(SoundEnum)
  }
];
