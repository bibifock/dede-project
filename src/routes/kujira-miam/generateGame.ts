import random from 'lodash/random';
import set from 'lodash/set';
import { z } from 'zod';

import { generateSounds } from './generateSounds';

import type { Game } from './types';

const NonEmptyString = z.string().trim().nonempty();

const DurationIntervalSchema = z
  .object({
    min: z.number().nonnegative(),
    max: z.number().nonnegative()
  })
  .refine((data) => data.min <= data.max, {
    message:
      'La valeur minimale doit être inférieure ou égale à la valeur maximale'
  });

const SettingsFormSchema = z
  .object({
    nbTarget: z.number().min(1),
    target: NonEmptyString,
    noises: NonEmptyString.array(),
    nbSounds: z.number().min(2),
    reward: NonEmptyString,
    soundDuration: z.number().positive(),
    breakDuration: DurationIntervalSchema,
    countdown: z.number().nonnegative()
  })
  .refine((data) => data.nbTarget < data.nbSounds, {
    message: 'Le nombre de sons doit être supérieur au nombre de sons cible',
    path: ['nbSounds']
  });

type SettingsForm = z.infer<typeof SettingsFormSchema>;

export function generateGame(settingsForm: Partial<SettingsForm>): {
  game?: Game;
  errors?: Record<string, string>;
} {
  const result = SettingsFormSchema.safeParse(settingsForm);
  if (!result.success) {
    return {
      errors: result.error.issues.reduce((acc, { path, message }) => {
        const newAcc = { ...acc };
        set(newAcc, path.join('.'), message);
        return newAcc;
      }, {})
    };
  }

  const {
    nbTarget,
    target,
    nbSounds,
    noises,
    reward,
    soundDuration: sound,
    breakDuration,
    countdown
  } = result.data;
  return {
    game: {
      target,
      reward,
      breaks: Array.from(Array(nbSounds - 1), () =>
        random(breakDuration.min, breakDuration.max)
      ),
      sounds: generateSounds({ nbTarget, target, nbSounds, noises }),
      durations: {
        sound,
        break: breakDuration,
        countdown
      }
    }
  };
}
