import { describe, it, expect } from 'vitest';

import { generateSounds } from './generateSounds';

describe('generateSounds', () => {
  it('should always display right number of target', () => {
    const sounds = generateSounds({
      nbTarget: 1,
      target: 'target',
      noises: ['sound1', 'sound2', 'sound3', 'target'],
      nbSounds: 4
    });

    expect(sounds.length).toEqual(4);

    expect(sounds.filter((v) => v === 'target').length).toEqual(1);
  });

  it('should avoid consecutive target sounds', () => {
    const sounds = generateSounds({
      nbTarget: 3,
      target: 'target',
      noises: ['sound1', 'sound2', 'sound3', 'target'],
      nbSounds: 5
    });

    expect(sounds).toEqual([
      'target',
      expect.stringContaining('sound'),
      'target',
      expect.stringContaining('sound'),
      'target'
    ]);
  });
});
