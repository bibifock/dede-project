export function getSoundURL(sound: string) {
  return `/sounds/${sound.toUpperCase()}.mp3`;
}
