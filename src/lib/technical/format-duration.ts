import { formatDuration as formatDurationDateFns } from 'date-fns';
import { fr } from 'date-fns/locale';

export function formatDuration(time: number) {
  const minutes = Math.floor(time / 1000 / 60);
  const seconds = Math.floor((time / 1000 / 60 - minutes) * 60);
  return formatDurationDateFns({ minutes, seconds }, { locale: fr });
}
