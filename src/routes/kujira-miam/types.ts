import type { IconSource } from 'svelte-hero-icons';

export enum EActionType {
  START = 'start',
  SUCCESS = 'success',
  ERROR = 'error',
  END = 'end'
}

export const ActionTypes = EActionType;

export interface IAction {
  gameTime: number;
  type: EActionType;
  icon?: IEventIcon;
  sound?: string;
  soundTime: number;
}

export type TResult = Array<IAction>;

export type DurationInterval = { min: number; max: number };

export type SettingsDurations = {
  sound: number;
  break: DurationInterval;
  countdown: number;
};

export interface ISettings {
  target: string;
  noises: string[];
  reward: string;
  nbTarget: number;
  nbSounds: number;
  durations: SettingsDurations;
}

export type Game = {
  sounds: string[];
  breaks: number[];
  target: string;
  reward: string;
  durations: SettingsDurations;
};

export interface IEventIcon {
  src: IconSource;
  color: string;
}

export interface IEvent {
  icon: IEventIcon;
  time: string;
}

export interface IHistoryAction {
  isTarget: boolean;
  icon: IEventIcon;
  time: string;
  sound: string;
  events: Array<IEvent>;
  type: EActionType;
}

export interface IKujiraMiam {
  game?: Game;
  result?: TResult;
}
