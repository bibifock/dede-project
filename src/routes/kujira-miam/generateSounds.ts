import { arrayRandomize } from '$lib/technical/array-randomize';

type GenerateSoundsArgs = {
  nbTarget: number;
  target: string;
  noises: string[];
  nbSounds: number;
};

export function generateSounds({
  nbTarget,
  target,
  noises,
  nbSounds
}: GenerateSoundsArgs) {
  const sounds = Array(nbTarget).fill(target);
  const noisesList = arrayRandomize(noises.filter((s) => s !== target));
  for (let i = 0; i < nbSounds - nbTarget; i++) {
    sounds.push(noisesList[i % noisesList.length]);
  }

  // add some random sorting
  sounds.reverse().sort(() => Math.random() - 0.5);

  const finalList = [];
  let nbTargetStay = nbTarget;
  while (finalList.length < nbSounds) {
    const lastInsert = finalList[finalList.length - 1];
    const forceTarget =
      lastInsert !== target && nbTargetStay * 2 > sounds.length;

    const foundIndex = sounds.findIndex((v) =>
      forceTarget ? v === target : v !== lastInsert
    );
    const currentIndex = foundIndex > -1 ? foundIndex : 0;
    if (sounds[currentIndex] === target) {
      nbTargetStay--;
    }

    finalList.push(sounds.splice(foundIndex > -1 ? foundIndex : 0, 1)[0]);
  }

  return finalList;
}
