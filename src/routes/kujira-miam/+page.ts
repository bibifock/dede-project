import type { PageLoad } from './$types';

export const load = (({ url }) => {
  return {
    reload: !!url.searchParams.get('reolad')
  };
}) satisfies PageLoad;
